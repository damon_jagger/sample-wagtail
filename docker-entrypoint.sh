if [ "$DB_FORCE_MIGRATE" = "Y" ]; then
    echo "Running migrations"
    python manage.py migrate
fi

if [ "$FORCE_ADMIN_CREATE" = "Y" ]; then
    echo "Creating Wagtail Superuser"
    echo "from django.contrib.auth.models import User; User.objects.filter(email='${WAGTAIL_EMAIL}').delete(); User.objects.create_superuser('${WAGTAIL_USER}', '${WAGTAIL_EMAIL}', '${WAGTAIL_PASS}')" | python manage.py shell
fi

exec "$@"  