#!/bin/bash

set -e # this will exit with non zero if commands fail
set -x # print commands before exec

python manage.py makemigrations --check
python manage.py test --no-input