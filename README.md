# Wagtail Sample

This repository contains a sample wagtail site for demo purposes.

## Getting started 

### Prerequisites

1. Install Docker on the host machine
2. Clone this repository

### Startup

1. Change to the repository directory
2. `docker-compose up`

### Stopping

1. `docker-compose down`

## Environment Variables

Defaults are set in `docker-compose.yml`