from django.db import models

from wagtail.wagtailcore.models import Page
from wagtail.wagtailadmin.edit_handlers import FieldPanel, MultiFieldPanel

class ConditionPage(Page):

    description = models.CharField(max_length=255, blank=True)

    content_panels = Page.content_panels + [
        MultiFieldPanel(
            [
                FieldPanel('description')
            ],
            heading="Condition Information",
            classname="collapsible"
        ),
    ]

    class Meta:
        verbose_name = 'Condition Page'