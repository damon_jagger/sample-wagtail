FROM python:3

RUN apt-get update && apt-get install -y \
	git

WORKDIR /wagtail

COPY requirements.txt .
RUN pip install -r requirements.txt

COPY . .

EXPOSE 80 8000

ENTRYPOINT ["bash", "docker-entrypoint.sh"]

CMD ["/usr/local/bin/uwsgi", "--ini", "/wagtail/docker-support/uwsgi.ini"]